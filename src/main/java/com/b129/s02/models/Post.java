package com.b129.s02.models;

import javax.persistence.*;

@Entity
@Table(name = "posts")

public class Post {

    // Properties

    @Id                     // primary key
    @GeneratedValue         // auto-incremented
    private Long id;

    @Column                 // field
    private String title;

    @Column                 // field
    private String content;

    // Constructors
    public Post() {}

    public Post(String title, String content ) {
        this.title = title;
        this.content = content;
    }

    // Getters and Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
