package com.b129.s02.models;

import javax.persistence.*;

@Entity
@Table(name = "users")

public class User {

    // Properties

    @Id                     // primary key
    @GeneratedValue         // auto-incremented
    private Long id;

    @Column                 // field
    private String username;

    @Column                 // field
    private String password;

    // Constructors
    public User() {}

    public User(String username, String password ) {
        this.username = username;
        this.password = password;
    }

    // Getters and Setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

